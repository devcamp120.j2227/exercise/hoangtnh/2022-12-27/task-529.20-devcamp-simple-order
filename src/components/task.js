import { Button, Container, Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { quantityHandle, buttonDetailHandler } from "../actions/task.action";
import routerList from "../routes";
const Task = () =>{
    // B3: Khai báo dispatch để đẩy action tới reducer
    const dispatch = useDispatch();
    //B1: nhận giá trị khởi tạo của state trong giai đoạn mở đầu
    const {mobileList} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    //set giá trị điều hướng url param
    const navigate = useNavigate();

    //tính tổng giá tiền đơn hàng dựa trên số lượng hàng đã chọn
    const total = mobileList.reduce((summary, item) => summary+ item.price*item.quantity, 0);
    //B2:
    const onButtonBuyClickHandler = (value, id) =>{
        dispatch(quantityHandle(id));
    }
    const onButtonDetailClickHandler = (value) =>{
        dispatch(buttonDetailHandler(value));
        navigate("/detail");
    }  
    return (
        <Container>
            <Grid container spacing={5} mt={5} style={{padding:"0px"}}>
                {mobileList.map((value,index) =>{
                    return (
                    < Grid item xs={4} key={index}>
                        <Grid container rowSpacing={1} sx={{
                        width: 320,
                        height: 300,
                        border:"1px solid"}} style={{display:"flex",alignItems: "center"}}>
                            <Grid style={{marginLeft:"50px", marginBottom:"10px"}}>
                                <h4>{value.name}</h4>
                                <p>{value.price}</p>
                                <p >Quantity:{value.quantity}</p>
                                <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <Button style={{backgroundColor:"green", color:"white"}} onClick={()=>onButtonBuyClickHandler(value, index)}>Buy</Button>
                                </Grid>
                                    <Grid item xs={6}>
                                        <Button style={{backgroundColor:"green", color:"white"}} onClick={()=>onButtonDetailClickHandler(value, routerList.path)}>Detail</Button>
                                    </Grid>
                                </Grid>
                                
                            </Grid>
                        </Grid>
                    </Grid>
                    )   
                })}
            </Grid>
            <Grid>
                <h4>Total: {total} USD</h4>
            </Grid>
        </Container>
        
    )
}
export default Task;