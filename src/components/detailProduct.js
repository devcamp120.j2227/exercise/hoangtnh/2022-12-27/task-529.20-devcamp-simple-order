import { useSelector } from "react-redux"
import { Button, Container, Grid } from "@mui/material";
import { useNavigate } from "react-router-dom";

const DetailProduct = () =>{
    const {detailProduct} = useSelector((reduxData) =>
        reduxData.taskReducer
    )
    const navigate = useNavigate();
    const onButtonBackClickHandler = () =>{
        navigate("/");
    }
    return (
        <>
        {detailProduct !== null ?
        <Container>
            <Grid container spacing={5} mt={5} style={{padding:"0px"}}>
                < Grid item xs={4}>
                    <Grid container rowSpacing={1} sx={{
                    width: 320,
                    height: 300,
                    border:"1px solid"}} style={{display:"flex",alignItems: "center"}}>
                        <Grid style={{marginLeft:"50px", marginBottom:"10px"}}>
                            <h4>{detailProduct.name}</h4>
                            <p>{detailProduct.price}</p>
                            <p >Quantity:{detailProduct.quantity}</p>
                            <Button style={{backgroundColor:"green", color:"white"}} onClick={()=>onButtonBackClickHandler()}>Back</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Container> 
        : null}
        </>
    )
}
export default DetailProduct;