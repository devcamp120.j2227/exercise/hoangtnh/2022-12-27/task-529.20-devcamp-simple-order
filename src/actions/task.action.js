import { BUTTON_DETAIL_HANDLER, TASK_QUANTITY_HANDLER } from "../constants/task.constant";
//mô tả những sự kiện liên quan tới task
const quantityHandle = (data)=>{
    console.log("action", data)
    return {
        type: TASK_QUANTITY_HANDLER,
        payload: data,
    }
}
const buttonDetailHandler = (data) =>{
    return {
        type: BUTTON_DETAIL_HANDLER,
        payload: data
    }
}

export {
    quantityHandle,
    buttonDetailHandler,
}