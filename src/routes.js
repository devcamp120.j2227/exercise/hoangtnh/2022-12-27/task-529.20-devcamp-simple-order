import DetailProduct from "./components/detailProduct";
import Task from "./components/task";
import ProductPage from "./pages/detailproduct";
import HomePage from "./pages/homepage";


const routerList = [
    {path: "/", element : <HomePage/>},
    {path: "/detail", element: <ProductPage/>}
]
export default routerList;