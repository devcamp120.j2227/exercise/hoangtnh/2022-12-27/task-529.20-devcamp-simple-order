import Content from "./views/Content";

function App() {
  return (
    <div>
      <Content/>
    </div>
  );
}

export default App;
