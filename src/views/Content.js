import routes from "../routes";
import { Route, Routes } from "react-router-dom";
import Task from "../components/task"

const Content =() =>{
    return(
        <>
            <Routes>
                {routes.map((router, index) =>{

                    return(
                    <Route key = {index} exact path ={router.path} element={router.element}>

                    </Route>
                    )
                })}
                <Route path="*" element={<Task />}></Route>
            </Routes>
        </>
    )
}
export default Content;